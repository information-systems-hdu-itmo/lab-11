<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register</title>
</head>
<body>
	<div>
		<form:form method="POST" modelAttribute="userForm">
			<h2>Registration</h2>
			<div>
				<form:input type="text" path="username" placeholder="Username"
					autofocus="true" />
				<form:errors path="username" />
			</div>
			<div>
				<form:input type="password" path="password" placeholder="Password" />
				<form:errors path="password" />
			</div>
			<div>
				<form:input type="password" path="passwordConfirm"
					placeholder="Confirm your password" />
			</div>
			<input type="submit" value="Register" />
		</form:form>
	</div>
</body>
</html>