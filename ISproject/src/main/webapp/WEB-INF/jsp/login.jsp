<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>
	<form method="POST" action="/login">
		<h2>Login</h2>
		<div>
			<input type="text" name="username" placeholder="Username" autofocus="true" />
			<input type="password" name="password" placeholder="Password" />
			<input type="submit" value="Log In" />
			<h4><a href="/registration">Register</a></h4>
		</div>
	</form>
</body>
</html>     

