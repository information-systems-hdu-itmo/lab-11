package com.example.isproject;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.example.isproject.db.entities.Role;
import com.example.isproject.internals.UsersService;

@Component
public class IsProjectAppDataInitializier implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadyInitialized = false;

    private final UsersService usersService;

    public IsProjectAppDataInitializier(UsersService usersSvc) {
        usersService = usersSvc;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (!alreadyInitialized) {
            alreadyInitialized = true;
            this.initializeApp();
        }

    }

    private void initializeApp() {
        if (usersService.isAdminNotExists("admin")) {
            usersService.registerNewUser("admin", "password", Role.ADMIN);
        }
    }
}
