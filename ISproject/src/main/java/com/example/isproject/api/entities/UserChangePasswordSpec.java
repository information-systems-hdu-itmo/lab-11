package com.example.isproject.api.entities;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserChangePasswordSpec {

    private static final String errorTooShortStringMessage = "{min} or more characters needed";

    @NotBlank
    @Size(min = 5, message = errorTooShortStringMessage)
    private String oldPassword;


    @NotBlank
    @Size(min = 5, message = errorTooShortStringMessage)
    private String password;

    @NotBlank
    @Size(min = 5, message = errorTooShortStringMessage)
    private String passwordConfirm;

    public UserChangePasswordSpec() {

    }

    public UserChangePasswordSpec(String oldPassword, String password, String passwordConfirm) {
        this.oldPassword = oldPassword;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setUsername(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
