package com.example.isproject.api.entities;

import java.util.List;

import org.springframework.data.domain.Page;

import com.example.isproject.db.entities.Dish;
import com.example.isproject.internals.PageSpec;

public class DishesList {

    public class PageInfo {
        public final PageSpec spec;
        public final long number, size;

        public PageInfo(PageSpec spec, long number, long size) {
            this.spec = spec;
            this.number = number;
            this.size = size;
        }

        public PageSpec getSpec() {
            return spec;
        }

        public long getNumber() {
            return number;
        }

        public long getSize() {
            return size;
        }
    }

    private final PageInfo _page;
    private final long _totalItems, _totalPages;
    private final List<Dish> _items;

    private DishesList(PageSpec spec, Page<Dish> page) {
        _page = spec == null || !spec.applied ? null : new PageInfo(spec, page.getNumber(), page.getSize());
        _totalItems = page.getTotalElements();
        _totalPages = page.getTotalPages();
        _items = page.getContent();
    }


    public static DishesList forResultOf(PageSpec spec, Page<Dish> result) {
        return new DishesList(spec, result);
    }


    public PageInfo getPage() {
        return _page;
    }

    public long getTotalItems() {
        return _totalItems;
    }

    public long getTotalPages() {
        return _totalPages;
    }

    public List<Dish> getItems() {
        return _items;
    }
}
