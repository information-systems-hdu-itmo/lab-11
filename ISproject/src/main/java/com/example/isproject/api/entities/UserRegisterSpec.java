package com.example.isproject.api.entities;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserRegisterSpec {

    private static final String errorTooShortStringMessage = "{min} or more characters needed";

    @NotBlank
    @Size(min = 2, message = errorTooShortStringMessage)
    private String username;

    @NotBlank
    @Size(min = 5, message = errorTooShortStringMessage)
    private String password;

    @NotBlank
    @Size(min = 5, message = errorTooShortStringMessage)
    private String passwordConfirm;

    public UserRegisterSpec() {

    }

    public UserRegisterSpec(String username, String password, String passwordConfirm) {
        this.username = username;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }


}
