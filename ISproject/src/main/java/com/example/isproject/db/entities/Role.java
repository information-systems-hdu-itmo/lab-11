package com.example.isproject.db.entities;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {

    ADMIN(0L),
    MANAGER(1L),
    USER(2L);

    private final long id;

    private Role(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    @Override
    public String getAuthority() {
        return "ROLE " + this.name();
    }
}