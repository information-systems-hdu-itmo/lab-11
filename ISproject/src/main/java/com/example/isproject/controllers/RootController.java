package com.example.isproject.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.example.isproject.db.entities.Role;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.isproject.api.entities.UserRegisterSpec;
import com.example.isproject.internals.UsersService;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping("/")
public class RootController {
    private final UsersService userService;

    public RootController(UsersService userService) {
        this.userService = userService;
    }

//	@GetMapping("")
//	public ResponseEntity<Object> resolveDefaultLocation(HttpServletRequest request){
//		String acceptTypeString = request.getHeader(HttpHeaders.ACCEPT);
//		if (acceptTypeString != null) {
//			List<MimeType> expectedMimeTypes = MimeTypeUtils.parseMimeTypes(acceptTypeString);
//			if (expectedMimeTypes.stream().anyMatch(t -> t.equalsTypeAndSubtype(MimeTypeUtils.TEXT_HTML))) {
//			return ResponseEntity.status(HttpStatus.FOUND).location(URI.create("/dishes?view=html")).build();
//			}
//		}
//		return ResponseEntity.status(HttpStatus.FOUND).location(URI.create("/dishes")).build();
//	}

    @GetMapping("")
    public ModelAndView resolveDefaultLocation() {
        return new ModelAndView("index");
    }


    @GetMapping("/registration")
    public ModelAndView getRegistrationHtmlForm() {
        return new ModelAndView("registration", "userForm", new UserRegisterSpec());
    }

    @PostMapping("/registration")
    public ModelAndView registerNewUser(@ModelAttribute("userForm") @Valid UserRegisterSpec userForm, BindingResult bindingResult) {

        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
            bindingResult.rejectValue("password", "error.password.confirm-not-matched");
        }

        if (!bindingResult.hasErrors()) {
            if (userService.registerNewUser(userForm.getUsername(), userForm.getPassword(), Role.USER)) {
                return new ModelAndView("redirect:/");
            } else {
                bindingResult.rejectValue("username", "error.user.already-exists");
            }
        }

        ModelAndView result = new ModelAndView("registration", "userForm", userForm);
        result.setStatus(HttpStatus.BAD_REQUEST);
        return result;
    }
}
