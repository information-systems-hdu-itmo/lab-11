package com.example.isproject.internals;

import java.util.List;
import java.util.Optional;

import com.example.isproject.db.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.isproject.db.entities.User;
import com.example.isproject.db.repositories.UsersRepository;

@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public boolean registerNewUser(String username, String password, Role role) {
        Optional<User> oldUser = usersRepository.findByUsername(username);
        if (oldUser.isPresent()) {
            return false;
        }
        String encodedPassword = passwordEncoder.encode(password);
        User newUser = new User(null, encodedPassword, username, role);
        usersRepository.save(newUser);
        return true;
    }

    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = usersRepository.findByUsername(username);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }

        return user.get();
    }

    public boolean isAdminNotExists(String adminUsername) {
        return usersRepository.findAll((r, q, b) -> b.or(
            b.equal(r.get("role"), Role.ADMIN),
            b.equal(r.get("username"), adminUsername)
        )).isEmpty();
    }
}




